Inicialización proyecto
git config --global user.name "Alejandro León"
git config --global user.email "juanalejandrol15@gmail.com"

Create a new repository
git clone https://gitlab.com/s04g4sucursalvirtual/e-boutique.git
cd e-boutique
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/s04g4sucursalvirtual/e-boutique.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/s04g4sucursalvirtual/e-boutique.git
git push -u origin --all
git push -u origin --tags

git log ->muestra todos los comming que estan hechos
git branch -a ->para verificar las ramas
git branch -D (nombre de la rama) ->eliminar rama